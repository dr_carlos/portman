portman-conf(8)
==============

Name
----
portman-conf - query portman's configuration file


Synopsis
--------
'portman-conf' [options] [<directive> ...]

'portman-conf' (--repo-list|--help|--version)


Description
-----------
'portman-conf' is a utility for parsing the 'portman' configuration file
and returning script-friendly output. It is designed to properly handle
non-trivial configuration features such as variable interpolation and
the use of the Include directive, and guarantees that it will return the
same configuration values which 'portman' itself would use.

'portman-conf' will output the fully-resolved contents of the
configuration file by default, or, if provided with the name of a
configuration directive, output the contents of the given directive alone.


Options
-------
*-c, \--config* <path>::
	Specify an alternate configuration file.

*-R, \--rootdir* <path>::
	Specify an alternate installation root (default is `/`).

*-r, \--repo* <repository>::
	Query options for a specific repository.

*-v, \--verbose*::
	Always shown directive names.

*-l, \--repo-list*::
	List configured repositories.

*-h, \--help*::
	Output syntax and command line options.

*-V, \--version*::
	Display version and exit.


Examples
--------

portman-conf -r core Usage::
	Show the value of the Usage configuration option for core repository.

portman-conf Architecture::
	Show the value of the HoldPkg configuration option.


See Also
--------
linkman:portman.conf[5]

include::footer.asciidoc[]
