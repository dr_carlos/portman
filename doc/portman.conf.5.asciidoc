portman.conf(5)
==============

Name
----
portman.conf - portman package manager configuration file


Synopsis
--------
{sysconfdir}/portman.conf


Description
-----------
Portman, using linkman:libalpm[3], will attempt to read portman.conf each time it
is invoked. This configuration file is divided into sections or repositories.
Each section defines a package repository that portman can use when searching
for packages in '\--sync' mode. The exception to this is the options section,
which defines global options.

Comments are only supported by beginning a line with the hash (#) symbol.
Comments cannot begin in the middle of a line.


Example
-------

--------
#
# portman.conf
#
[options]
RootDir = /

[core]
Include = /etc/portman.d/core

[custom]
Server = file:///home/pkgs
--------

NOTE: Each directive must be in CamelCase. If the case isn't respected, the
directive won't be recognized. For example. noupgrade or NOUPGRADE will not
work.


Options
-------
*RootDir =* /path/to/root/dir::
	Set the default root directory for portman to install to. This option is
	used if you want to install a package on a temporary mounted partition
	which is "owned" by another system, or for a chroot install.
	*NOTE*: If database path or log file are not specified on either the
	command line or in linkman:portman.conf[5], their default location will
	be inside this root path.

*CacheDir =* /path/to/cache/dir::
	Overrides the default location of the package cache directory. The
	default is +{localstatedir}/cache/portman/pkg/+. Multiple cache directories can be
	specified, and they are tried in the order they are listed in the config
	file. If a file is not found in any cache directory, it will be downloaded
	to the first cache directory with write access. *NOTE*: this is an absolute
	path, the root path is not automatically prepended.

*LogFile =* /path/to/log/file::
	Overrides the default location of the portman log file. The default
	is +{localstatedir}/log/portman.log+. This is an absolute path and the root directory
	is not prepended.

*Include =* /path/to/config/file::
	Include another configuration file. This file can include repositories or
	general configuration options. Wildcards in the specified paths will get
	expanded based on linkman:glob[7] rules.

*Architecture =* auto &| i686 &| x86_64 | ...::
	If set, portman will only allow installation of packages with the given
	architectures (e.g. 'i686', 'x86_64', etc). The special value 'auto' will
	use the system architecture, provided via ``uname -m''. If unset, no
	architecture checks are made. *NOTE*: Packages with the special
	architecture 'any' can always be installed, as they are meant to be
	architecture independent.

*CleanMethod =* KeepInstalled &| KeepCurrent::
	If set to `KeepInstalled` (the default), the '-Sc' operation will clean
	packages that are no longer installed (not present in the local database).
	If set to `KeepCurrent`, '-Sc' will clean outdated packages (not present in
	any sync database).
	The second behavior is useful when the package cache is shared among
	multiple machines, where the local databases are usually different, but the
	sync databases in use could be the same. If both values are specified,
	packages are only cleaned if not installed locally and not present in any
	known sync database.

*UseSyslog*::
	Log action messages through syslog(). This will insert log entries into
	+{localstatedir}/log/messages+ or equivalent.

*Color*::
	Automatically enable colors only when portman's output is on a tty.

*NoProgressBar*::
	Disables progress bars. This is useful for terminals which do
	not support escape characters.

*CheckSpace*::
	Performs an approximate check for adequate available disk space before
	installing packages.

*VerbosePkgLists*::
	Displays name, version and size of target packages formatted
	as a table for upgrade, sync and remove operations.

*DisableDownloadTimeout*::
	Disable defaults for low speed limit and timeout on downloads. Use this
	if you have issues downloading files with proxy and/or security gateway.


Repository Sections
-------------------
Each repository section defines a section name and at least one location where
the packages can be found. The section name is defined by the string within
square brackets (the two above are 'core'  and  'custom'). Repository names
must be unique and the name 'local' is reserved for the database of installed
packages. Locations are defined with the 'Server' directive and follow a URL
naming structure. If you want to use a local directory, you can specify the
full path with a ``file://'' prefix, as shown above.

A common way to define DB locations utilizes the 'Include' directive. For each
repository defined in the configuration file, a single 'Include' directive can
contain a file that lists the servers for that repository.

--------
[core]
# use this server first
Server = ftp://ftp.archlinux.org/$repo/os/$arch
# next use servers as defined in the mirrorlist below
Include = {sysconfdir}/portman.d/mirrorlist
--------

The order of repositories in the configuration files matters; repositories
listed first will take precedence over those listed later in the file when
packages in two repositories have identical names, regardless of version
number.

*Include =* path::
	Include another config file. This file can include repositories or
	general configuration options. Wildcards in the specified paths will get
	expanded based on linkman:glob[7] rules.

*Server =* url::
	A full URL to a location where the database, packages, and signatures (if
	available) for this repository can be found.
+
During parsing, portman will define the `$repo` variable to the name of the
current section. This is often utilized in files specified using the 'Include'
directive so all repositories can use the same mirrorfile. portman also defines
the `$arch` variable to the first (or only) value of the `Architecture` option,
so the same mirrorfile can even be used for different architectures.

*Usage =* ...::
	Set the usage level for this repository. This option takes a list of tokens
	which must be at least one of the following:
		*Sync*;;
			Enables refreshes for this repository.
		*Search*;;
			Enables searching for this repository.
		*Install*;;
			Enables installation of packages from this repository during a '\--sync'
			operation.
		*Upgrade*;;
			Allows this repository to be a valid source of packages when performing
			a '\--sysupgrade'.
		*All*;;
			Enables all of the above features for the repository. This is the default
			if not specified.
+
Note that an enabled repository can be operated on explicitly, regardless of the Usage
level set.


See Also
--------
linkman:portman[8], linkman:libalpm[3]

include::footer.asciidoc[]
