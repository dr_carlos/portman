# build behavior
option('use-git-version', type : 'boolean', value : false,
       description : 'take version information from git')
option('buildstatic', type : 'boolean', value : false,
       description : 'if true, build statically linked binaries')

# directories and filenames
option('root-dir', type : 'string', value : '/',
       description : 'set the location of the root operating directory')

option('ldconfig', type : 'string', value : '/sbin/ldconfig',
       description : 'set the full path to ldconfig')

option('datarootdir', type : 'string', value : 'share',
       description : 'FIXME')

option('debug-suffix', type : 'string', value : 'debug',
       description : 'suffix for split debugging symbol packages used by makepkg')

# dependencies, features
option('doc', type : 'feature', value : 'auto',
       description : 'generate docs and manpages')

option('doxygen', type : 'feature', value : 'disabled',
       description : 'generate doxygen manpages and html')

option('i18n', type : 'boolean', value : true,
       description : 'enable localization of portman')

option('gentoolkit', type : 'boolean', value : true,
       description : 'enable reverse dependencies through gentoolkit')

option('python-version', type : 'string', value : '3.10',
       description : 'set python version used')

# tools
option('file-seccomp', type: 'feature', value: 'auto',
	   description: 'determine whether file is seccomp-enabled')
