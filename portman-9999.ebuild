# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8..11} )

inherit git-r3 python-single-r1

EGIT_REPO_URI="https://gitlab.com/dr_carlos/portman.git"
DESCRIPTION="A pacman-like interface to Gentoo's Portage package manager"
HOMEPAGE="https://gitlab.com/dr_carlos/portman"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

IUSE="nls doc doxygen +gentoolkit"
REQUIRED_USE="
	doxygen? ( doc )
	${PYTHON_REQUIRED_USE}
	"
RDEPEND="
	${PYTHON_DEPS}
	curl? ( net-misc/curl )
	$(python_gen_cond_dep '
		>=sys-apps/portage-3.0.0[${PYTHON_USEDEP}]
		gentoolkit? ( >=app-portage/gentoolkit-0.5.0[${PYTHON_USEDEP}] )
	')
	"

BDEPEND="
	${PYTHON_DEPS}
	nls? ( virtual/libintl )
	doxygen? ( app-doc/doxygen )
	"

src_configure() {
	meson build

	if use nls; then
		meson configure -Di18n=true build
	else
		meson configure -Di18n=false build
	fi

	if use doc; then
		meson configure -Ddoc=enabled build
	else
		meson configure -Ddoc=disabled build
	fi

	if use doxygen; then
		meson configure -Ddoxygen=enabled build
	else
		meson configure -Ddoxygen=disabled build
	fi

	if use gentoolkit; then
		meson configure -Dgentoolkit=true build
	else
		meson configure -Dgentoolkit=false build
	fi

	meson configure -Dpython-version="${EPYTHON#*n}" build
}

src_compile() {
	ninja -C build
}

src_install() {
	DESTDIR="${D}" ninja -C build install
}
