/*
 *  list.c
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

/* Note: list.{c,h} are intended to be standalone files. Do not include
 * any other libalpm headers.
 */

/* libalpm */
#include "list.h"

/* check exported library symbols with: nm -C -D <lib> */
#define SYMEXPORT __attribute__((visibility("default")))

/* Allocation */

void SYMEXPORT list_free(list_t *list) {
  list_t *it = list;

  while (it) {
    list_t *tmp = it->next;
    free(it);
    it = tmp;
  }
}

void SYMEXPORT list_free_inner(list_t *list, list_fn_free fn) {
  list_t *it = list;

  if (fn) {
    while (it) {
      if (it->data) {
        fn(it->data);
      }
      it = it->next;
    }
  }
}

/* Mutators */

list_t SYMEXPORT *list_add(list_t *list, void *data) {
  list_append(&list, data);
  return list;
}

list_t SYMEXPORT *list_append(list_t **list, void *data) {
  list_t *ptr;

  ptr = malloc(sizeof(list_t));
  if (ptr == NULL) {
    return NULL;
  }

  ptr->data = data;
  ptr->next = NULL;

  /* Special case: the input list is empty */
  if (*list == NULL) {
    *list = ptr;
    ptr->prev = ptr;
  } else {
    list_t *lp = list_last(*list);
    lp->next = ptr;
    ptr->prev = lp;
    (*list)->prev = ptr;
  }

  return ptr;
}

list_t SYMEXPORT *list_append_strdup(list_t **list, const char *data) {
  list_t *ret;
  char *dup;
  if ((dup = strdup(data)) && (ret = list_append(list, dup))) {
    return ret;
  } else {
    free(dup);
    return NULL;
  }
}

list_t SYMEXPORT *list_add_sorted(list_t *list, void *data, list_fn_cmp fn) {
  if (!fn || !list) {
    return list_add(list, data);
  } else {
    list_t *add = NULL, *prev = NULL, *next = list;

    add = malloc(sizeof(list_t));
    if (add == NULL) {
      return list;
    }
    add->data = data;

    /* Find insertion point. */
    while (next) {
      if (fn(add->data, next->data) <= 0)
        break;
      prev = next;
      next = next->next;
    }

    /* Insert the add node to the list */
    if (prev == NULL) { /* special case: we insert add as the first element */
      add->prev = list->prev; /* list != NULL */
      add->next = list;
      list->prev = add;
      return add;
    } else if (next == NULL) { /* another special case: add last element */
      add->prev = prev;
      add->next = NULL;
      prev->next = add;
      list->prev = add;
      return list;
    } else {
      add->prev = prev;
      add->next = next;
      next->prev = add;
      prev->next = add;
      return list;
    }
  }
}

list_t SYMEXPORT *list_join(list_t *first, list_t *second) {
  list_t *tmp;

  if (first == NULL) {
    return second;
  }
  if (second == NULL) {
    return first;
  }
  /* tmp is the last element of the first list */
  tmp = first->prev;
  /* link the first list to the second */
  tmp->next = second;
  /* link the second list to the first */
  first->prev = second->prev;
  /* set the back reference to the tail */
  second->prev = tmp;

  return first;
}

list_t SYMEXPORT *list_mmerge(list_t *left, list_t *right, list_fn_cmp fn) {
  list_t *newlist, *lp, *tail_ptr, *left_tail_ptr, *right_tail_ptr;

  if (left == NULL) {
    return right;
  }
  if (right == NULL) {
    return left;
  }

  /* Save tail node pointers for future use */
  left_tail_ptr = left->prev;
  right_tail_ptr = right->prev;

  if (fn(left->data, right->data) <= 0) {
    newlist = left;
    left = left->next;
  } else {
    newlist = right;
    right = right->next;
  }
  newlist->prev = NULL;
  newlist->next = NULL;
  lp = newlist;

  while ((left != NULL) && (right != NULL)) {
    if (fn(left->data, right->data) <= 0) {
      lp->next = left;
      left->prev = lp;
      left = left->next;
    } else {
      lp->next = right;
      right->prev = lp;
      right = right->next;
    }
    lp = lp->next;
    lp->next = NULL;
  }
  if (left != NULL) {
    lp->next = left;
    left->prev = lp;
    tail_ptr = left_tail_ptr;
  } else if (right != NULL) {
    lp->next = right;
    right->prev = lp;
    tail_ptr = right_tail_ptr;
  } else {
    tail_ptr = lp;
  }

  newlist->prev = tail_ptr;

  return newlist;
}

list_t SYMEXPORT *list_msort(list_t *list, size_t n, list_fn_cmp fn) {
  if (n > 1) {
    size_t half = n / 2;
    size_t i = half - 1;
    list_t *left = list, *lastleft = list, *right;

    while (i--) {
      lastleft = lastleft->next;
    }
    right = lastleft->next;

    /* tidy new lists */
    lastleft->next = NULL;
    right->prev = left->prev;
    left->prev = lastleft;

    left = list_msort(left, half, fn);
    right = list_msort(right, n - half, fn);
    list = list_mmerge(left, right, fn);
  }
  return list;
}

list_t SYMEXPORT *list_remove_item(list_t *haystack, list_t *item) {
  if (haystack == NULL || item == NULL) {
    return haystack;
  }

  if (item == haystack) {
    /* Special case: removing the head node which has a back reference to
     * the tail node */
    haystack = item->next;
    if (haystack) {
      haystack->prev = item->prev;
    }
    item->prev = NULL;
  } else if (item == haystack->prev) {
    /* Special case: removing the tail node, so we need to fix the back
     * reference on the head node. We also know tail != head. */
    if (item->prev) {
      /* i->next should always be null */
      item->prev->next = item->next;
      haystack->prev = item->prev;
      item->prev = NULL;
    }
  } else {
    /* Normal case, non-head and non-tail node */
    if (item->next) {
      item->next->prev = item->prev;
    }
    if (item->prev) {
      item->prev->next = item->next;
    }
  }

  return haystack;
}

list_t SYMEXPORT *list_remove(list_t *haystack, const void *needle,
                              list_fn_cmp fn, void **data) {
  list_t *i = haystack;

  if (data) {
    *data = NULL;
  }

  if (needle == NULL) {
    return haystack;
  }

  while (i) {
    if (i->data == NULL) {
      i = i->next;
      continue;
    }
    if (fn(i->data, needle) == 0) {
      haystack = list_remove_item(haystack, i);

      if (data) {
        *data = i->data;
      }
      free(i);
      break;
    } else {
      i = i->next;
    }
  }

  return haystack;
}

list_t SYMEXPORT *list_remove_str(list_t *haystack, const char *needle,
                                  char **data) {
  return list_remove(haystack, (const void *)needle, (list_fn_cmp)strcmp,
                     (void **)data);
}

list_t SYMEXPORT *list_remove_dupes(const list_t *list) {
  const list_t *lp = list;
  list_t *newlist = NULL;
  while (lp) {
    if (!list_find_ptr(newlist, lp->data)) {
      if (list_append(&newlist, lp->data) == NULL) {
        list_free(newlist);
        return NULL;
      }
    }
    lp = lp->next;
  }
  return newlist;
}

list_t SYMEXPORT *list_strdup(const list_t *list) {
  const list_t *lp = list;
  list_t *newlist = NULL;
  while (lp) {
    if (list_append_strdup(&newlist, lp->data) == NULL) {
      FREELIST(newlist);
      return NULL;
    }
    lp = lp->next;
  }
  return newlist;
}

list_t SYMEXPORT *list_copy(const list_t *list) {
  const list_t *lp = list;
  list_t *newlist = NULL;
  while (lp) {
    if (list_append(&newlist, lp->data) == NULL) {
      list_free(newlist);
      return NULL;
    }
    lp = lp->next;
  }
  return newlist;
}

list_t SYMEXPORT *list_copy_data(const list_t *list, size_t size) {
  const list_t *lp = list;
  list_t *newlist = NULL;
  while (lp) {
    void *newdata = malloc(size);
    if (newdata) {
      memcpy(newdata, lp->data, size);
      if (list_append(&newlist, newdata) == NULL) {
        free(newdata);
        FREELIST(newlist);
        return NULL;
      }
      lp = lp->next;
    } else {
      FREELIST(newlist);
      return NULL;
    }
  }
  return newlist;
}

list_t SYMEXPORT *list_reverse(list_t *list) {
  const list_t *lp;
  list_t *newlist = NULL, *backup;

  if (list == NULL) {
    return NULL;
  }

  lp = list_last(list);
  /* break our reverse circular list */
  backup = list->prev;
  list->prev = NULL;

  while (lp) {
    if (list_append(&newlist, lp->data) == NULL) {
      list_free(newlist);
      return NULL;
    }
    lp = lp->prev;
  }
  list->prev = backup; /* restore tail pointer */
  return newlist;
}

/* Accessors */

list_t SYMEXPORT *list_nth(const list_t *list, size_t n) {
  const list_t *i = list;
  while (n--) {
    i = i->next;
  }
  return (list_t *)i;
}

inline list_t SYMEXPORT *list_next(const list_t *node) {
  if (node) {
    return node->next;
  } else {
    return NULL;
  }
}

inline list_t SYMEXPORT *list_previous(const list_t *list) {
  if (list && list->prev->next) {
    return list->prev;
  } else {
    return NULL;
  }
}

list_t SYMEXPORT *list_last(const list_t *list) {
  if (list) {
    return list->prev;
  } else {
    return NULL;
  }
}

/* Misc */

size_t SYMEXPORT list_count(const list_t *list) {
  size_t i = 0;
  const list_t *lp = list;
  while (lp) {
    ++i;
    lp = lp->next;
  }
  return i;
}

void SYMEXPORT *list_find(const list_t *haystack, const void *needle,
                          list_fn_cmp fn) {
  const list_t *lp = haystack;
  while (lp) {
    if (lp->data && fn(lp->data, needle) == 0) {
      return lp->data;
    }
    lp = lp->next;
  }
  return NULL;
}

/* trivial helper function for list_find_ptr */
static int ptr_cmp(const void *p, const void *q) { return (p != q); }

void SYMEXPORT *list_find_ptr(const list_t *haystack, const void *needle) {
  return list_find(haystack, needle, ptr_cmp);
}

char SYMEXPORT *list_find_str(const list_t *haystack, const char *needle) {
  return (char *)list_find(haystack, (const void *)needle, (list_fn_cmp)strcmp);
}

void SYMEXPORT list_diff_sorted(const list_t *left, const list_t *right,
                                list_fn_cmp fn, list_t **onlyleft,
                                list_t **onlyright) {
  const list_t *l = left;
  const list_t *r = right;

  if (!onlyleft && !onlyright) {
    return;
  }

  while (l != NULL && r != NULL) {
    int cmp = fn(l->data, r->data);
    if (cmp < 0) {
      if (onlyleft) {
        *onlyleft = list_add(*onlyleft, l->data);
      }
      l = l->next;
    } else if (cmp > 0) {
      if (onlyright) {
        *onlyright = list_add(*onlyright, r->data);
      }
      r = r->next;
    } else {
      l = l->next;
      r = r->next;
    }
  }
  while (l != NULL) {
    if (onlyleft) {
      *onlyleft = list_add(*onlyleft, l->data);
    }
    l = l->next;
  }
  while (r != NULL) {
    if (onlyright) {
      *onlyright = list_add(*onlyright, r->data);
    }
    r = r->next;
  }
}

list_t SYMEXPORT *list_diff(const list_t *lhs, const list_t *rhs,
                            list_fn_cmp fn) {
  list_t *left, *right;
  list_t *ret = NULL;

  left = list_copy(lhs);
  left = list_msort(left, list_count(left), fn);
  right = list_copy(rhs);
  right = list_msort(right, list_count(right), fn);

  list_diff_sorted(left, right, fn, &ret, NULL);

  list_free(left);
  list_free(right);
  return ret;
}

void SYMEXPORT *list_to_array(const list_t *list, size_t n, size_t size) {
  size_t i;
  const list_t *item;
  char *array;

  if (n == 0) {
    return NULL;
  }

  array = malloc(n * size);
  if (array == NULL) {
    return NULL;
  }
  for (i = 0, item = list; i < n && item; i++, item = item->next) {
    memcpy(array + i * size, item->data, size);
  }
  return array;
}
