/*
 *  list.h
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdlib.h> /* size_t */

/**
 * @ingroup libalpm
 * @addtogroup list list(3)
 * @brief Functions to manipulate list_t lists.
 *
 * These functions are designed to create, destroy, and modify lists of
 * type list_t. This is an internal list type used by libalpm that is
 * publicly exposed for use by frontends if desired.
 *
 * It is exposed so front ends can use it to prevent the need to reimplement
 * lists of their own; however, it is not required that the front end uses
 * it.
 * @{
 */

/** A doubly linked list */
typedef struct _list_t {
  /** data held by the list node */
  void *data;
  /** pointer to the previous node */
  struct _list_t *prev;
  /** pointer to the next node */
  struct _list_t *next;
} list_t;

/** Frees a list and its contents */
#define FREELIST(p)                                                            \
  do {                                                                         \
    list_free_inner(p, free);                                                  \
    list_free(p);                                                              \
    p = NULL;                                                                  \
  } while (0)

/** item deallocation callback.
 * @param item the item to free
 */
typedef void (*list_fn_free)(void *item);

/** item comparison callback */
typedef int (*list_fn_cmp)(const void *, const void *);

/* allocation */

/** Free a list, but not the contained data.
 *
 * @param list the list to free
 */
void list_free(list_t *list);

/** Free the internal data of a list structure but not the list itself.
 *
 * @param list the list to free
 * @param fn a free function for the internal data
 */
void list_free_inner(list_t *list, list_fn_free fn);

/* item mutators */

/** Add a new item to the end of the list.
 *
 * @param list the list to add to
 * @param data the new item to be added to the list
 *
 * @return the resultant list
 */
list_t *list_add(list_t *list, void *data);

/**
 * @brief Add a new item to the end of the list.
 *
 * @param list the list to add to
 * @param data the new item to be added to the list
 *
 * @return the newly added item
 */
list_t *list_append(list_t **list, void *data);

/**
 * @brief Duplicate and append a string to a list.
 *
 * @param list the list to append to
 * @param data the string to duplicate and append
 *
 * @return the newly added item
 */
list_t *list_append_strdup(list_t **list, const char *data);

/**
 * @brief Add items to a list in sorted order.
 *
 * @param list the list to add to
 * @param data the new item to be added to the list
 * @param fn   the comparison function to use to determine order
 *
 * @return the resultant list
 */
list_t *list_add_sorted(list_t *list, void *data, list_fn_cmp fn);

/**
 * @brief Join two lists.
 * The two lists must be independent. Do not free the original lists after
 * calling this function, as this is not a copy operation. The list pointers
 * passed in should be considered invalid after calling this function.
 *
 * @param first  the first list
 * @param second the second list
 *
 * @return the resultant joined list
 */
list_t *list_join(list_t *first, list_t *second);

/**
 * @brief Merge the two sorted sublists into one sorted list.
 *
 * @param left  the first list
 * @param right the second list
 * @param fn    comparison function for determining merge order
 *
 * @return the resultant list
 */
list_t *list_mmerge(list_t *left, list_t *right, list_fn_cmp fn);

/**
 * @brief Sort a list of size `n` using mergesort algorithm.
 *
 * @param list the list to sort
 * @param n    the size of the list
 * @param fn   the comparison function for determining order
 *
 * @return the resultant list
 */
list_t *list_msort(list_t *list, size_t n, list_fn_cmp fn);

/**
 * @brief Remove an item from the list.
 * item is not freed; this is the responsibility of the caller.
 *
 * @param haystack the list to remove the item from
 * @param item the item to remove from the list
 *
 * @return the resultant list
 */
list_t *list_remove_item(list_t *haystack, list_t *item);

/**
 * @brief Remove an item from the list.
 *
 * @param haystack the list to remove the item from
 * @param needle   the data member of the item we're removing
 * @param fn       the comparison function for searching
 * @param data     output parameter containing data of the removed item
 *
 * @return the resultant list
 */
list_t *list_remove(list_t *haystack, const void *needle, list_fn_cmp fn,
                    void **data);

/**
 * @brief Remove a string from a list.
 *
 * @param haystack the list to remove the item from
 * @param needle   the data member of the item we're removing
 * @param data     output parameter containing data of the removed item
 *
 * @return the resultant list
 */
list_t *list_remove_str(list_t *haystack, const char *needle, char **data);

/**
 * @brief Create a new list without any duplicates.
 *
 * This does NOT copy data members.
 *
 * @param list the list to copy
 *
 * @return a new list containing non-duplicate items
 */
list_t *list_remove_dupes(const list_t *list);

/**
 * @brief Copy a string list, including data.
 *
 * @param list the list to copy
 *
 * @return a copy of the original list
 */
list_t *list_strdup(const list_t *list);

/**
 * @brief Copy a list, without copying data.
 *
 * @param list the list to copy
 *
 * @return a copy of the original list
 */
list_t *list_copy(const list_t *list);

/**
 * @brief Copy a list and copy the data.
 * Note that the data elements to be copied should not contain pointers
 * and should also be of constant size.
 *
 * @param list the list to copy
 * @param size the size of each data element
 *
 * @return a copy of the original list, data copied as well
 */
list_t *list_copy_data(const list_t *list, size_t size);

/**
 * @brief Create a new list in reverse order.
 *
 * @param list the list to copy
 *
 * @return a new list in reverse order
 */
list_t *list_reverse(list_t *list);

/* item accessors */

/**
 * @brief Return nth element from list (starting from 0).
 *
 * @param list the list
 * @param n    the index of the item to find (n < list_count(list) IS needed)
 *
 * @return an list_t node for index `n`
 */
list_t *list_nth(const list_t *list, size_t n);

/**
 * @brief Get the next element of a list.
 *
 * @param list the list node
 *
 * @return the next element, or NULL when no more elements exist
 */
list_t *list_next(const list_t *list);

/**
 * @brief Get the previous element of a list.
 *
 * @param list the list head
 *
 * @return the previous element, or NULL when no previous element exist
 */
list_t *list_previous(const list_t *list);

/**
 * @brief Get the last item in the list.
 *
 * @param list the list
 *
 * @return the last element in the list
 */
list_t *list_last(const list_t *list);

/* misc */

/**
 * @brief Get the number of items in a list.
 *
 * @param list the list
 *
 * @return the number of list items
 */
size_t list_count(const list_t *list);

/**
 * @brief Find an item in a list.
 *
 * @param needle   the item to search
 * @param haystack the list
 * @param fn       the comparison function for searching (!= NULL)
 *
 * @return `needle` if found, NULL otherwise
 */
void *list_find(const list_t *haystack, const void *needle, list_fn_cmp fn);

/**
 * @brief Find an item in a list.
 *
 * Search for the item whose data matches that of the `needle`.
 *
 * @param needle   the data to search for (== comparison)
 * @param haystack the list
 *
 * @return `needle` if found, NULL otherwise
 */
void *list_find_ptr(const list_t *haystack, const void *needle);

/**
 * @brief Find a string in a list.
 *
 * @param needle   the string to search for
 * @param haystack the list
 *
 * @return `needle` if found, NULL otherwise
 */
char *list_find_str(const list_t *haystack, const char *needle);

/**
 * @brief Find the differences between list `left` and list `right`
 *
 * The two lists must be sorted. Items only in list `left` are added to the
 * `onlyleft` list. Items only in list `right` are added to the `onlyright`
 * list.
 *
 * @param left      the first list
 * @param right     the second list
 * @param fn        the comparison function
 * @param onlyleft  pointer to the first result list
 * @param onlyright pointer to the second result list
 *
 */
void list_diff_sorted(const list_t *left, const list_t *right, list_fn_cmp fn,
                      list_t **onlyleft, list_t **onlyright);

/**
 * @brief Find the items in list `lhs` that are not present in list `rhs`.
 *
 * @param lhs the first list
 * @param rhs the second list
 * @param fn  the comparison function
 *
 * @return a list containing all items in `lhs` not present in `rhs`
 */

list_t *list_diff(const list_t *lhs, const list_t *rhs, list_fn_cmp fn);

/**
 * @brief Copy a list and data into a standard C array of fixed length.
 * Note that the data elements are shallow copied so any contained pointers
 * will point to the original data.
 *
 * @param list the list to copy
 * @param n    the size of the list
 * @param size the size of each data element
 *
 * @return an array version of the original list, data copied as well
 */
void *list_to_array(const list_t *list, size_t n, size_t size);
