/*
 *  deptest.c
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "list.h"

/* portman */
#include "conf.h"
#include "portman.h"

// explanation: portman -T pkg pkg>=version
// it will print out whatever arguments or 'targets' are not installed
int portman_deptest(list_t *targets) {
  /*
  list_t *i;
  list_t *deps = NULL;
  alpm_db_t *localdb = alpm_get_localdb(config->handle);
  list_t *pkgcache = alpm_db_get_pkgcache(localdb);

  for (i = targets; i; i = list_next(i)) {
    char *target = i->data;

    if (!alpm_db_get_pkg(localdb, target) &&
        !alpm_find_satisfier(pkgcache, target)) {
      deps = list_add(deps, target);
    }
  }

  if (deps == NULL) {
    return 0;
  }

  for (i = deps; i; i = list_next(i)) {
    const char *dep = i->data;

    printf("%s\n", dep);
  }
  list_free(deps);
  return 127;
  */

  return 0;
}
