#pragma once

typedef enum _alpm_loglevel_t {
  /** Error */
  ALPM_LOG_ERROR = 1,
  /** Warning */
  ALPM_LOG_WARNING = (1 << 1),
  /** Debug */
  ALPM_LOG_DEBUG = (1 << 2),
  /** Function */
  ALPM_LOG_FUNCTION = (1 << 3)
} alpm_loglevel_t;
