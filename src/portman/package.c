/*
 *  package.c
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <wchar.h>

#include "list.h"

/* portman */
#include "conf.h"
#include "package.h"
#include "portman.h"
#include "util.h"

#define CLBUF_SIZE 4096

char titles[_T_MAX][TITLE_MAXLEN * sizeof(wchar_t)];

/** Build the `titles` array of localized titles and pad them with spaces so
 * that they align with the longest title. Storage for strings is stack
 * allocated and naively truncated to TITLE_MAXLEN characters.
 */
void make_aligned_titles(void) { 
   unsigned int i; 
   size_t maxlen = 0; 
   int maxcol = 0; 
   static const wchar_t title_suffix[] = L" :"; 
   wchar_t wbuf[ARRAYSIZE(titles)][TITLE_MAXLEN + ARRAYSIZE(title_suffix)] = {
 
       {0}}; 
   size_t wlen[ARRAYSIZE(wbuf)]; 
   int wcol[ARRAYSIZE(wbuf)]; 
   char *buf[ARRAYSIZE(wbuf)]; 
   buf[T_ARCHITECTURE] = _("Architecture"); 
   buf[T_BACKUP_FILES] = _("Backup Files"); 
   buf[T_BUILD_DATE] = _("Build Date"); 
   buf[T_BUILD_DEPS] = _("Build Deps"); 
   buf[T_COMPRESSED_SIZE] = _("Compressed Size"); 
   buf[T_CONFLICTS_WITH] = _("Conflicts With"); 
   buf[T_DEPENDS_ON] = _("Depends On"); 
   buf[T_DESCRIPTION] = _("Description"); 
   buf[T_DOWNLOAD_SIZE] = _("Download Size"); 
   buf[T_CATEGORY] = _("Category"); 
   buf[T_INSTALL_DATE] = _("Install Date"); 
   buf[T_INSTALL_REASON] = _("Install Reason"); 
   buf[T_INSTALL_SCRIPT] = _("Install Script"); 
   buf[T_INSTALLED_SIZE] = _("Installed Size"); 
   buf[T_LICENSES] = _("Licenses"); 
   buf[T_MD5_SUM] = _("MD5 Sum"); 
   buf[T_NAME] = _("Name"); 
   buf[T_OPTIONAL_DEPS] = _("Optional Deps"); 
   buf[T_OPTIONAL_FOR] = _("Optional For"); 
   buf[T_PACKAGER] = _("Packager"); 
   buf[T_PROVIDES] = _("Provides"); 
   buf[T_REPLACES] = _("Replaces"); 
   buf[T_REPOSITORY] = _("Repository"); 
   buf[T_REQUIRED_BY] = _("Required By"); 
   buf[T_SHA_256_SUM] = _("SHA-256 Sum"); 
   buf[T_SIGNATURES] = _("Signatures"); 
   buf[T_URL] = _("URL"); 
   buf[T_VALIDATED_BY] = _("Validated By"); 
   buf[T_VERSION] = _("Version"); 

   for (i = 0; i < ARRAYSIZE(wbuf); i++) { 
     wlen[i] = mbstowcs(wbuf[i], buf[i], strlen(buf[i]) + 1); 
     wcol[i] = wcswidth(wbuf[i], wlen[i]); 
     if (wcol[i] > maxcol) { 
       maxcol = wcol[i]; 
     } 
     if (wlen[i] > maxlen) { 
       maxlen = wlen[i]; 
     } 
   } 

   for (i = 0; i < ARRAYSIZE(wbuf); i++) { 
     size_t padlen = maxcol - wcol[i]; 
     wmemset(wbuf[i] + wlen[i], L' ', padlen); 
     wmemcpy(wbuf[i] + wlen[i] + padlen, title_suffix, ARRAYSIZE(title_suffix)); 
     wcstombs(titles[i], wbuf[i], sizeof(wbuf[i])); 
   } 
 } 

/** Turn a depends list into a text list.
 * @param deps a list with items of type alpm_depend_t
 */
/* static void deplist_display(const char *title, list_t *deps, */
/*                             unsigned short cols) { */
/*   list_t *i, *text = NULL; */
/* for (i = deps; i; i = list_next(i)) { */
/*   alpm_depend_t *dep = i->data; */
/*   text = list_add(text, alpm_dep_compute_string(dep)); */
/* } */
/*   list_display(title, text, cols); */
/*   FREELIST(text); */
/* } */

/** Turn a optdepends list into a text list.
 * @param optdeps a list with items of type alpm_depend_t
 */
/* static void optdeplist_display(alpm_pkg_t *pkg, unsigned short cols) { */
/*   list_t *i, *text = NULL; */
/*   alpm_db_t *localdb = alpm_get_localdb(config->handle); */
/*   for (i = alpm_pkg_get_optdepends(pkg); i; i = list_next(i)) { */
/*     alpm_depend_t *optdep = i->data; */
/*     char *depstring = alpm_dep_compute_string(optdep); */
/*     if (alpm_pkg_get_origin(pkg) == ALPM_PKG_FROM_LOCALDB) { */
/*       if (alpm_find_satisfier(alpm_db_get_pkgcache(localdb), depstring)) { */
/*         const char *installed = _(" [installed]"); */
/*         depstring = */
/*             realloc(depstring, strlen(depstring) + strlen(installed) + 1); */
/*         strcpy(depstring + strlen(depstring), installed); */
/*       } */
/*     } */
/*     text = list_add(text, depstring); */
/*   } */
/*   list_display_linebreak(titles[T_OPTIONAL_DEPS], text, cols); */
/*   FREELIST(text); */
/* } */

static const char* get_repo_pkg_info(const char* atom, const char* key) {
	PyObject* info = PyObject_CallMethod(
				PyObject_GetAttrString(
					tree("porttree"),
					"dbapi"
				),
				"aux_get",
				"s [s]",
				atom,
				key
			 );

	if (info == NULL)
		return NULL;

	const char* ret = PyUnicode_AsUTF8(PyList_GetItem(info, 0));
	Py_DECREF(info);

	if (ret[0] == 0)
		return NULL;

	return ret;
}

static const char* get_local_pkg_info(const char* atom, const char* key) {
	PyObject* info = PyObject_CallMethod(
				PyObject_GetAttrString(
					tree("vartree"),
					"dbapi"
				),
				"aux_get",
				"s [s]",
				atom,
				key
			 );

	if (info == NULL)
		return NULL;

	const char* ret = PyUnicode_AsUTF8(PyList_GetItem(info, 0));
	Py_DECREF(info);

	if (ret[0] == 0)
		return NULL;

	return ret;
}

static int pkg_in_world(const char* atom) {
	return 1 == PySet_Contains(
			PyDict_GetItemString(
				PyObject_GetAttrString(
					root_config,
					"sets"
				),
				"selected"
			),
			Py(atom)
		    );
}

static char* get_pkg_name(const char* atom) {
	char* new_atom = malloc(sizeof(char) * strlen(atom) + 1);
	strcpy(new_atom, atom);

	char* P = strchr(new_atom, '/') + 1;

	char* PV = strchr(P, '-');
	if (PV)
		*PV = 0;

	char* ret = malloc(sizeof(char) * strlen(P) + 1);
	strcpy(ret, P);
	free(new_atom);

	return ret;
}

static char* get_pkg_version(const char* atom) {
	char* new_atom = malloc(sizeof(char) * strlen(atom) + 1);
	strcpy(new_atom, atom);

	char* PV = strchr(strchr(new_atom, '/') + 1, '-') + 1;

	char* ret = malloc(sizeof(char) * strlen(PV) + 1);
	strcpy(ret, PV);
	free(new_atom);

	return ret;
}

static char* get_pkg_category(const char* atom) {
	char* CATEGORY = malloc(sizeof(char) * strlen(atom) + 1);
	strcpy(CATEGORY, atom);

	char* P = strchr(CATEGORY, '/');
	if (P)
		*P = 0;

	return CATEGORY;
}

static list_t* get_pkg_deps(const char* atom, const char* type) {
	PyObject* dep = PyObject_GetAttrString(
				portage,
				"dep"
			);

	PyObject* kwargs = PyDict_New();
	PyDict_SetItemString(
			kwargs,
			"uselist",
			PyUnicode_Split(
				PyObject_CallMethodOneArg(
					PyObject_GetAttrString(
						portage,
						"settings"
					),
					Py("get"),
					Py("USE")
				),
				NULL,
				-1
			)
	);

	PyObject* list_deps = PyObject_Call(
				PyObject_GetAttrString(
					dep,
					"use_reduce"
				),
				PyTuple_Pack(
					1,
					PyList_GetItem(
						PyObject_CallMethod(
							PyObject_GetAttrString(
								tree("porttree"),
								"dbapi"
							),
							"aux_get",
							"s [s]",
							atom,
							type
						),
						0
					)
				),
				kwargs
			      );

	PyObject* iter = PyObject_GetIter(list_deps);
	list_t* deps = NULL;

	if (iter != NULL) {
		PyObject* item;

		while ((item = PyIter_Next(iter))) {
			const char* itemStr;

			if (PyUnicode_Check(item) && strcmp(PyUnicode_AsUTF8(item), "||") == 0) {
				PyObject* prev_item = item;
				item = PyIter_Next(iter);

				itemStr = PyUnicode_AsUTF8(
						PyObject_CallMethodOneArg(
							dep,
							Py("human_readable_required_use"),
							PyObject_CallMethod(
								dep,
								"paren_enclose",
								"[O O]",
								prev_item,
								item
							)
						)
					  );

				Py_DECREF(prev_item);
			} else
				itemStr = PyUnicode_AsUTF8(
						PyObject_CallMethodOneArg(
							dep,
							Py("human_readable_required_use"),
							PyObject_CallMethod(
								dep,
								"paren_enclose",
								"[O]",
								item
							)
						)
					  );

			char* itemMutable = malloc(sizeof(char) * strlen(itemStr) + 1);
			strcpy(itemMutable, itemStr);

			list_append(
				&deps,
				itemMutable
			);

			Py_DECREF(item);
		}

		Py_DECREF(iter);
	}

	Py_DECREF(dep);
	Py_DECREF(kwargs);
	Py_DECREF(list_deps);
	
	return deps;
}

static list_t* get_pkg_rev_deps(const char* atom, char installedOnly) {
	PyObject* installed_pkgs_gen = PyObject_CallNoArgs(
					PyObject_GetAttrString(
						PyImport_AddModule("gentoolkit.helpers"),
		  				installedOnly ? "get_installed_cpvs" : "get_cpvs"
	     				)
				       );

	PyList_Sort(installed_pkgs_gen);

	PyObject* kwargs = PyDict_New();
	PyDict_SetItemString(
			kwargs,
			"pkgset",
			installed_pkgs_gen
	);

	PyObject* list_rev_deps = PyObject_Call(
					PyObject_GetAttrString(
						PyObject_CallMethodOneArg(
							PyImport_AddModule("gentoolkit.dependencies"),
							Py("Dependencies"),
							Py(atom)
						),
						"graph_reverse_depends"
					),
					PyTuple_New(0),
					kwargs
				  );

	PyObject* iter = PyObject_GetIter(list_rev_deps);
	list_t* rev_deps = NULL;

	if (iter != NULL) {
		PyObject* item;

		while ((item = PyIter_Next(iter))) {
			const char* itemStr = PyUnicode_AsUTF8(
						PyObject_GetAttrString(
							item,
							"cpv"
						)
					      );

			if (itemStr != NULL) {
				char* itemMutable = malloc(sizeof(char) * strlen(itemStr) + 1);

				if (itemMutable != NULL) {
					strcpy(itemMutable, itemStr);

					list_append(
						&rev_deps,
						itemMutable
					);
				}
			}

			Py_DECREF(item);
		}

		Py_DECREF(iter);
	}

	Py_DECREF(list_rev_deps);
	Py_DECREF(installed_pkgs_gen);
	Py_DECREF(kwargs);

	return rev_deps;
}

static list_t* get_pkg_conflicts(list_t** rdeps) {
	list_t* conflicts = NULL;

	list_t* pkg = *rdeps;
	while (pkg != NULL) {
		if (pkg->data != NULL && ((char *)pkg->data)[0] == '!') {
			if (((char *)pkg->data)[1] == '!') {
				// There's two less chars because of the !!, but there's 1 more because of the \0 at the end, so these balance out to -1.
				// Add 8 because of the bold and no bold modifiers
				int pkgLen = strlen(pkg->data);
				char* conflict = malloc(sizeof(char) * pkgLen + 7); 
				strcpy(conflict + 4, (char *)pkg->data + 2);
				conflict[0] = '\033';
				conflict[1] = '[';
				conflict[2] = '1';
				conflict[3] = 'm';

				conflict[pkgLen + 2] = '\033';
				conflict[pkgLen + 3] = '[';
				conflict[pkgLen + 4] = '2';
				conflict[pkgLen + 5] = '2';
				conflict[pkgLen + 6] = 'm';
				conflict[pkgLen + 7] = '\0';

				list_append(
					&conflicts,
					conflict
				);
			} else {
				// There's one less char because of the !, but there's 1 more because of the \0 at the end, so these balance out.
				char* conflict = malloc(sizeof(char) * strlen(pkg->data)); 
				strcpy(conflict, (char *)pkg->data + 1);

				list_append(
					&conflicts,
					conflict
				);
			}

			*rdeps = list_remove_item(*rdeps, pkg);

			list_t* removedPkg = pkg;
			pkg = pkg->next;
			free(removedPkg->data);
			free(removedPkg);
		} else
			pkg = pkg->next;
	}
	
	return conflicts;
}

/**
 * Display the details of a package.
 * Extra information entails 'required by' info for sync packages and backup
 * files info for local packages.
 * @param pkg package to display information for
 * @param extra should we show extra information
 */
void dump_pkg_full(const char* atom, int extra) {
  unsigned short cols;
  double size;
  char bdatestr[50] = "";
  const char *label, *reason;
  list_t *requiredby = NULL, *rdeps = NULL;

  // make aligned titles once only
  static int need_alignment = 1;
  if (need_alignment) {
    need_alignment = 0;
    make_aligned_titles();
  }

  // set variables here, do all output below
  if (config->op == PM_OP_QUERY) {
  	const char* bdate = get_local_pkg_info(atom, "BUILD_TIME");
	if (bdate) {
		char* ptr;
		time_t bdateUnix = (time_t) strtol(bdate, &ptr, 10);
		strftime(bdatestr, 50, "%c", localtime(&bdateUnix));
	}

	if (pkg_in_world(atom))
	    reason = _("Explicitly installed");
	else
	    reason = _("Installed as a dependency for another package");

	// compute this here so we don't get a pause in the middle of output
#ifdef ENABLE_GENTOOLKIT
	requiredby = get_pkg_rev_deps(atom, 1);
#endif
  }

#ifdef ENABLE_GENTOOLKIT
  if (extra && config->op == PM_OP_SYNC) {
	// compute this here so we don't get a pause in the middle of output
	printf("Computing reverse dependencies...\n");
	requiredby = get_pkg_rev_deps(atom, 0);
  }
#endif

  // compute this here so that conflicts can be calculated
  rdeps = get_pkg_deps(atom, "RDEPEND");

  cols = getcols();

  // actual output
  if (config->op == PM_OP_SYNC)
	string_display(titles[T_REPOSITORY], get_repo_pkg_info(atom, "repository"), cols);
  
  string_display_mut(titles[T_NAME], get_pkg_name(atom), cols);
  string_display_mut(titles[T_VERSION], get_pkg_version(atom), cols);
  string_display(titles[T_DESCRIPTION], get_repo_pkg_info(atom, "DESCRIPTION"), cols);
  string_display(titles[T_ARCHITECTURE], get_repo_pkg_info(atom, "KEYWORDS"), cols);
  string_display(titles[T_URL], get_repo_pkg_info(atom, "HOMEPAGE"), cols);
  string_display(titles[T_LICENSES], get_repo_pkg_info(atom, "LICENSE"), cols);
  string_display_mut(titles[T_CATEGORY], get_pkg_category(atom), cols);
  list_display(titles[T_CONFLICTS_WITH], get_pkg_conflicts(&rdeps), cols);
  list_display(titles[T_DEPENDS_ON], rdeps, cols);
  list_display(titles[T_BUILD_DEPS], get_pkg_deps(atom, "BDEPEND"), cols);

  
#ifdef ENABLE_GENTOOLKIT
  if (extra || config->op == PM_OP_QUERY)
    list_display(titles[T_REQUIRED_BY], requiredby, cols);
#endif

  if (config->op == PM_OP_QUERY) {
	  char* ptr;
	  size = humanize_size(strtol(get_local_pkg_info(atom, "SIZE"), &ptr, 10), 'M', 2, &label);
	  printf("%s%s%s %.2f %s\n", config->colstr.title, titles[T_INSTALLED_SIZE],
		 config->colstr.nocolor, size, label);
  }

  if (config->op == PM_OP_QUERY) {
    string_display(titles[T_BUILD_DATE], bdatestr, cols);
    string_display(titles[T_INSTALL_REASON], reason, cols);
  }

  // final newline to separate packages
  printf("\n");
}

/* static const char *get_backup_file_status(const char *root, */
/*                                           const alpm_backup_t *backup) { */
/*   char path[PATH_MAX]; */
/*   const char *ret; */

/*   snprintf(path, PATH_MAX, "%s%s", root, backup->name); */

/*   /\* if we find the file, calculate checksums, otherwise it is missing *\/
 */
/*   if (access(path, R_OK) == 0) { */
/*     char *md5sum = alpm_compute_md5sum(path); */

/*     if (md5sum == NULL) { */
/*       pm_printf(ALPM_LOG_ERROR, _("could not calculate checksums for %s\n"),
 */
/*                 path); */
/*       return NULL; */
/*     } */

/*     /\* if checksums don't match, file has been modified *\/ */
/*     if (strcmp(md5sum, backup->hash) != 0) { */
/*       ret = "[modified]"; */
/*     } else { */
/*       ret = "[unmodified]"; */
/*     } */
/*     free(md5sum); */
/*   } else { */
/*     switch (errno) { */
/*     case EACCES: */
/*       ret = "[unreadable]"; */
/*       break; */
/*     case ENOENT: */
/*       ret = "[missing]"; */
/*       break; */
/*     default: */
/*       ret = "[unknown]"; */
/*     } */
/*   } */
/*   return ret; */
/* } */

/* Display list of backup files and their modification states
 */
/* void dump_pkg_backups(alpm_pkg_t *pkg, unsigned short cols) { */
/*   list_t *i, *text = NULL; */
/*   const char *root = alpm_option_get_root(config->handle); */
/*   /\* package has backup files, so print them *\/ */
/*   for (i = alpm_pkg_get_backup(pkg); i; i = list_next(i)) { */
/*     const alpm_backup_t *backup = i->data; */
/*     const char *value; */
/*     char *line; */
/*     size_t needed; */
/*     if (!backup->hash) { */
/*       continue; */
/*     } */
/*     value = get_backup_file_status(root, backup); */
/*     needed = strlen(root) + strlen(backup->name) + 1 + strlen(value) + 1; */
/*     line = malloc(needed); */
/*     if (!line) { */
/*       goto cleanup; */
/*     } */
/*     sprintf(line, "%s%s %s", root, backup->name, value); */
/*     text = list_add(text, line); */
/*   } */

/*   list_display_linebreak(titles[T_BACKUP_FILES], text, cols); */

/* cleanup: */
/*   FREELIST(text); */
/* } */

/* List all files contained in a package
 */
/* void dump_pkg_files(alpm_pkg_t *pkg, int quiet) { */
/*   const char *pkgname, *root; */
/*   alpm_filelist_t *pkgfiles; */
/*   size_t i; */

/*   pkgname = alpm_pkg_get_name(pkg); */
/*   pkgfiles = alpm_pkg_get_files(pkg); */
/*   root = alpm_option_get_root(config->handle); */

/*   for (i = 0; i < pkgfiles->count; i++) { */
/*     const alpm_file_t *file = pkgfiles->files + i; */
/*     /\* Regular: '<pkgname> <root><filepath>\n' */
/*      * Quiet  : '<root><filepath>\n' */
/*      *\/ */
/*     if (!quiet) { */
/*       printf("%s%s%s ", config->colstr.title, pkgname,
 * config->colstr.nocolor); */
/*     } */
/*     printf("%s%s\n", root, file->name); */
/*   } */

/*   fflush(stdout); */
/* } */

/* Display the changelog of a package
 */
/* void dump_pkg_changelog(alpm_pkg_t *pkg) { */
/*   void *fp = NULL; */

/*   if ((fp = alpm_pkg_changelog_open(pkg)) == NULL) { */
/*     pm_printf(ALPM_LOG_ERROR, _("no changelog available for '%s'.\n"), */
/*               alpm_pkg_get_name(pkg)); */
/*     return; */
/*   } else { */
/*     fprintf(stdout, _("Changelog for %s:\n"), alpm_pkg_get_name(pkg)); */
/*     /\* allocate a buffer to get the changelog back in chunks *\/ */
/*     char buf[CLBUF_SIZE]; */
/*     size_t ret = 0; */
/*     while ((ret = alpm_pkg_changelog_read(buf, CLBUF_SIZE, pkg, fp))) { */
/*       fwrite(buf, 1, ret, stdout); */
/*     } */
/*     alpm_pkg_changelog_close(pkg, fp); */
/*     putchar('\n'); */
/*   } */
/* } */

/* void print_installed(alpm_db_t *db_local, alpm_pkg_t *pkg) { */
/*   const char *pkgname = alpm_pkg_get_name(pkg); */
/*   const char *pkgver = alpm_pkg_get_version(pkg); */
/*   alpm_pkg_t *lpkg = alpm_db_get_pkg(db_local, pkgname); */
/*   if (lpkg) { */
/*     const char *lpkgver = alpm_pkg_get_version(lpkg); */
/*     const colstr_t *colstr = &config->colstr; */
/*     if (strcmp(lpkgver, pkgver) == 0) { */
/*       printf(" %s[%s]%s", colstr->meta, _("installed"), colstr->nocolor); */
/*     } else { */
/*       printf(" %s[%s: %s]%s", colstr->meta, _("installed"), lpkgver, */
/*              colstr->nocolor); */
/*     } */
/*   } */
/* } */

/* void print_groups(alpm_pkg_t *pkg) { */
/*   list_t *grp; */
/*   if ((grp = alpm_pkg_get_groups(pkg)) != NULL) { */
/*     const colstr_t *colstr = &config->colstr; */
/*     list_t *k; */
/*     printf(" %s(", colstr->groups); */
/*     for (k = grp; k; k = list_next(k)) { */
/*       const char *group = k->data; */
/*       fputs(group, stdout); */
/*       if (list_next(k)) { */
/*         /\* only print a spacer if there are more groups *\/ */
/*         putchar(' '); */
/*       } */
/*     } */
/*     printf(")%s", colstr->nocolor); */
/*   } */
/* } */

/**
 * Display the details of a search.
 * @param db the database we're searching
 * @param targets the targets we're searching for
 * @param show_status show if the package is also in the local db
 * @return -1 on error, 0 if there were matches, 1 if there were not
 */
/* int dump_pkg_search(alpm_db_t *db, list_t *targets, int show_status) { */
/*   int freelist = 0; */
/*   alpm_db_t *db_local; */
/*   list_t *i, *searchlist = NULL; */
/*   unsigned short cols; */
/*   const colstr_t *colstr = &config->colstr; */

/*   if (show_status) { */
/*     db_local = alpm_get_localdb(config->handle); */
/*   } */

/*   /\* if we have a targets list, search for packages matching it *\/ */
/*   if (targets) { */
/*     if (alpm_db_search(db, targets, &searchlist) != 0) { */
/*       return -1; */
/*     } */
/*     freelist = 1; */
/*   } else { */
/*     searchlist = alpm_db_get_pkgcache(db); */
/*     freelist = 0; */
/*   } */
/*   if (searchlist == NULL) { */
/*     return 1; */
/*   } */

/*   cols = getcols(); */
/*   for (i = searchlist; i; i = list_next(i)) { */
/*     alpm_pkg_t *pkg = i->data; */

/*     if (config->quiet) { */
/*       fputs(alpm_pkg_get_name(pkg), stdout); */
/*     } else { */
/*       printf("%s%s/%s%s %s%s%s", colstr->repo, alpm_db_get_name(db), */
/*              colstr->title, alpm_pkg_get_name(pkg), colstr->version, */
/*              alpm_pkg_get_version(pkg), colstr->nocolor); */

/*       print_groups(pkg); */
/*       if (show_status) { */
/*         print_installed(db_local, pkg); */
/*       } */

/*       /\* we need a newline and initial indent first *\/ */
/*       fputs("\n    ", stdout); */
/*       indentprint(alpm_pkg_get_desc(pkg), 4, cols); */
/*     } */
/*     fputc('\n', stdout); */
/*   } */

/*   /\* we only want to free if the list was a search list *\/ */
/*   if (freelist) { */
/*     list_free(searchlist); */
/*   } */

/*   return 0; */
/* } */
