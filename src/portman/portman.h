/*
 *  portman.h
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <Python.h>
#include "list.h"

#define PORTMAN_CALLER_PREFIX "PORTMAN"

/* python macros */
#define Py(s) PyUnicode_DecodeFSDefault(s)
#define tree(t) PyObject_CallMethodOneArg(PyObject_GetAttrString(root_config, "trees"), PyUnicode_DecodeFSDefault("get"), PyUnicode_DecodeFSDefault(t))

/* python portage modules */
extern PyObject* portage;
extern PyObject* emerge_actions;

/* python objects */
extern PyObject* root_config;

/* query.c */
int portman_query(list_t *targets);
/* remove.c */
int portman_remove(list_t *targets);
/* sync.c */
int portman_sync(list_t *targets);
