/*
 *  sync.c
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dirent.h>
#include <errno.h>
#include <fnmatch.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#include "list.h"

/* portman */
#include "conf.h"
#include "log.h"
#include "package.h"
#include "portman.h"
#include "util.h"

static int unlink_verbose(const char *pathname, int ignore_missing) {
  int ret = unlink(pathname);
  if (ret) {
    if (ignore_missing && errno == ENOENT) {
      ret = 0;
    } else {
      pm_printf(ALPM_LOG_ERROR, _("could not remove %s: %s\n"), pathname,
                strerror(errno));
    }
  }
  return ret;
}

static int remove_dir(const char *path, int remove) {
   DIR *d = opendir(path);
   size_t path_len = strlen(path);
   int r = -1;

   if (d) {
      struct dirent *p;

      r = 0;
      while (!r && (p=readdir(d))) {
          int r2 = -1;
          char *buf;
          size_t len;

          /* Skip the names "." and ".." as we don't want to recurse on them. */
          if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
             continue;

          len = path_len + strlen(p->d_name) + 2; 
          buf = malloc(len);

          if (buf) {
             struct stat statbuf;

             snprintf(buf, len, "%s/%s", path, p->d_name);
             if (!lstat(buf, &statbuf)) {
                if (S_ISDIR(statbuf.st_mode))
                   r2 = remove_dir(buf, 1);
                else
		   r2 = unlink_verbose(buf, 0);
             }
             free(buf);
          }
          r = r2;
      }
      closedir(d);
   }

   if (!r && remove)
      r = rmdir(path);

   return r;
}

int portman_sync(list_t *targets) {
  list_t *sync_dbs = NULL;

  /* clean the cache */
  if (config->op_s_clean) {
	PyObject* tmpdir = PyObject_CallMethod(
				PyObject_GetAttrString(
					portage,
					"settings"
				),
				"get",
				"s",
				"PORTAGE_TMPDIR"
			   );

	const char* tmpdirStr = PyUnicode_AsUTF8(tmpdir);
	char* dir = malloc(strlen(tmpdirStr) + 9);
	sprintf(dir, "%s/portage", tmpdirStr);

	int ret = remove_dir(dir, 0);

	Py_DECREF(tmpdir);
	free(dir);

	return ret;
  }

  if (config->op_s_sync) {
	PyObject_CallMethod(
			PyObject_CallMethod(
				emerge_actions,
				"SyncRepos",
				NULL
			),
			"auto_sync",
			NULL
	);
  }

  /* search for a package */
  if (config->op_s_search) {
	PyObject* search_targets = PyList_New(0);

	list_t* target = targets;
	while (target != NULL) {
		PyList_Append(
				search_targets,
				Py(
					(const char *)target->data
				)
		);
		target = target->next;
	}

	if (PyList_Size(search_targets) == 0)
		PyList_Append(
				search_targets,
				Py("")
		);

	PyObject* args = PyTuple_Pack(
				6,
				root_config,
				Py_None,
				Py_False,
				PyBool_FromLong(
					!config->quiet
				),
				Py_False,
				Py_False
			 );

	PyObject* kwargs = PyDict_New();
	PyDict_SetItemString(kwargs, "search_index", Py_True);
	PyDict_SetItemString(kwargs, "search_similarity", Py_None);
	PyDict_SetItemString(kwargs, "fuzzy", Py_True);
	PyDict_SetItemString(kwargs, "regex_auto", Py_True);

        PyObject* search_instance = PyObject_Call(
					PyObject_GetAttrString(
						emerge_actions,
						"search"
					),
					args,
					kwargs
        			    );

	for (int i = 0; i < PyList_Size(search_targets); i++) {
		PyObject_CallMethodOneArg(
				search_instance, 
				Py("execute"),
				PyList_GetItem(
					search_targets,
					i
				)
		);

		PyObject_CallMethodNoArgs(
				search_instance, 
				Py("output")
		);
	}

        Py_DECREF(search_targets);
        Py_DECREF(args);
        Py_DECREF(kwargs);
        Py_DECREF(search_instance);
	return 0;
  }

  /* look for categories */
  if (config->category) {
	PyObject* cats = PySequence_List(
				PyObject_GetAttrString(
					PyObject_GetAttrString(
						tree("porttree"),
						"dbapi"
					),
					"categories"
				)
			 );

	PyList_Sort(cats);

	PyObject* iter = PyObject_GetIter(cats);
	PyObject* item;
	Py_DECREF(cats);

	while ((item = PyIter_Next(iter))) {
		printf("%s\n", PyUnicode_AsUTF8(item));
		Py_DECREF(item);
	}

	Py_DECREF(iter);

	return PyErr_Occurred() != NULL;
  }

  /* get package info */
  if (config->op_s_info) {
	if (targets == NULL) {
		PyObject* cpvs = PyObject_CallMethodNoArgs(
					PyObject_GetAttrString(
						tree("porttree"),
						"dbapi"
					),
					Py("cp_all")
				 );
		PyList_Sort(cpvs);

		PyObject* iter = PyObject_GetIter(cpvs);
		PyObject* item;

		while ((item = PyIter_Next(iter))) {
			const char* itemStr = PyUnicode_AsUTF8(item);

			char* itemMutable = malloc(sizeof(char) * strlen(itemStr) + 1);
			strcpy(itemMutable, itemStr);

			list_append(
				&targets,
				itemMutable
			);

			Py_DECREF(item);
		};

		Py_DECREF(iter);
		Py_DECREF(cpvs);
	}

	list_t* target = targets;
	while (target != NULL) {
		PyObject* matches = PyObject_CallMethodOneArg(
					PyObject_GetAttrString(
						tree("porttree"),
						"dbapi"
					),
					Py("match"),
					Py((const char*) target->data)
				    );

		if (PyErr_Occurred()) {
			PyObject *type, *value, *traceback, *pkg;
			PyErr_Fetch(&type, &value, &traceback);
			
			PyObject* pkgs = PyObject_GetIter(
						PyTuple_GetItem(
							PyObject_GetAttrString(
								value,
								"args"
							),
							0
						)
					 );

			pm_printf(ALPM_LOG_ERROR, _("'%s' is too ambiguous, possible matches:\n"), (const char *) target->data);
			while ((pkg = PyIter_Next(pkgs))) {
				printf("- %s\n", PyUnicode_AsUTF8(pkg));
				Py_DECREF(pkg);
			}

			Py_DECREF(pkgs);
			Py_DECREF(type);
			Py_DECREF(value);
			Py_DECREF(traceback);
		}

		if (matches != NULL) {
			int len = PyList_Size(matches);

			if (len == 0)
				pm_printf(ALPM_LOG_ERROR, _("no packages match '%s'\n"), (const char *) target->data);
			else {
				const char* atom = PyUnicode_AsUTF8(PyList_GetItem(matches, len - 1));

				dump_pkg_full(atom, config->op_s_info > 1);

				// if we DECREF before using atom it will free atom
				Py_DECREF(matches);
			}
		}

		target = target->next;
	}

	return 0;
  }

  /* get a listing of files in sync DBs */
  if (config->op_q_list) {
	if (targets == NULL) {
		PyObject* iter = PyObject_GetIter(
					PyObject_CallMethodNoArgs(
						PyObject_GetAttrString(
							PyObject_GetAttrString(
								tree("porttree"),
								"dbapi"
							),
							"treemap"
						),
						Py("keys")
					)
				 );

		PyObject* item;
		while ((item = PyIter_Next(iter))) {
			const char* itemStr = PyUnicode_AsUTF8(item);
			char* itemMutable = malloc(sizeof(char) * strlen(itemStr) + 1);
			strcpy(itemMutable, itemStr);

			list_append(
				&targets,
				itemMutable

			);

			Py_DECREF(item);
		}

		Py_DECREF(iter);
	}

	DIR *d;
	size_t path_len;

	list_t* target = targets;
	while (target != NULL) {
		const char* path = PyUnicode_AsUTF8(
					PyObject_CallMethodOneArg(
						PyObject_GetAttrString(
							PyObject_GetAttrString(
								PyObject_GetAttrString(
									tree("porttree"),
									"dbapi"
								),
								"repositories"
							),
							"treemap"
						),
						Py("get"),
						Py((char *) target->data)
					)
				   );

		d = opendir(path);
		path_len = strlen(path);

		if (d) {
		   struct dirent *p;

		   while ((p=readdir(d))) {
		       char *buf;
		       size_t len;

		       /* Skip the names ".", "..", and .* (e.g. .git) as we don't want to recurse on them. */
		       if ((p->d_name)[0] == '.')
			  continue;

		       len = path_len + strlen(p->d_name) + 2; 
		       buf = malloc(len);

		       if (buf) {
			  struct stat statbuf;

			  snprintf(buf, len, "%s/%s", path, p->d_name);
			  if (!lstat(buf, &statbuf)) {
			     if (S_ISDIR(statbuf.st_mode)) {
				DIR* cat = opendir(buf);

				if (cat) {
				   struct dirent *p2;

				   while ((p2=readdir(cat))) {
				       char *cpv_path;
				       size_t len2;

				       /* Skip the names ".", "..", and .* (e.g. .git) as we don't want to recurse on them. */
				       if ((p2->d_name)[0] == '.')
					  continue;

				       len2 = strlen(buf) + strlen(p2->d_name) + 2; 
				       cpv_path = malloc(len2);

				       if (buf) {
					  struct stat statbuf2;

					  snprintf(cpv_path, len2, "%s/%s", buf, p2->d_name);
					  if (!lstat(cpv_path, &statbuf2)) {
					     if (S_ISDIR(statbuf2.st_mode)) {
						int cpv_len = strlen(p->d_name) + strlen(p2->d_name) + 2;
						char* cpv = malloc(cpv_len);
						snprintf(cpv, cpv_len, "%s/%s", p->d_name, p2->d_name);

						PyObject* iter = PyObject_GetIter(
									PyObject_CallMethodOneArg(
										PyObject_GetAttrString(
											tree("porttree"),
											"dbapi"
										),
										Py("cp_list"),
										Py(cpv)
									)
								 );
						PyObject* item;

						while ((item = PyIter_Next(iter))) {
							printf("%s::%s\n", PyUnicode_AsUTF8(item), (char *) target->data);
							Py_DECREF(item);
						}

						Py_DECREF(iter);
						free(cpv);
					     }
					  }
					  free(cpv_path);
				       }
				   }

				   closedir(cat);
			     }
			     }
			  }
			  free(buf);
		       }
		   }

		   closedir(d);
		}

		target = target->next;
	}

	return 0;
  }

  PyObject* syncOpts = PyDict_New();
  PyObject* args = PyList_New(0);

  if (targets == NULL) {
    if (config->op_s_upgrade) {
	printf("Calculating dependencies...\n");

	PyDict_SetItemString(syncOpts, "--update", Py_True);
	PyDict_SetItemString(syncOpts, "--deep", Py_True);
	PyDict_SetItemString(syncOpts, "--changed-use", Py_True);

	PyList_Append(args, Py("@world"));
    } else if (config->op_s_sync) {
      return 0;
    } else {
      /* don't proceed here unless we have an operation that doesn't require a
       * target list */
      pm_printf(ALPM_LOG_ERROR, _("no targets specified (use -h for help)\n"));
      return 1;
    }
  } else {
	list_t* target = targets;

	while (target != NULL) {
		PyList_Append(args, Py((char *)target->data));

		target = target->next;
	}
  }

  PyDict_SetItemString(syncOpts, "--ask", Py_True);
  PyDict_SetItemString(syncOpts, "--verbose", Py_True);
  PyDict_SetItemString(syncOpts, "--regex-search-auto", Py("y"));

  // install/update packages
  PyObject* kwargs = PyDict_New();

  PyDict_SetItemString(kwargs, "opts", syncOpts);
  PyDict_SetItemString(kwargs, "args", args);

  PyObject_CallMethodOneArg(
		  emerge_actions,
		  Py("action_build"),
		  PyObject_Call(
			  PyObject_GetAttrString(
				  emerge_actions,
				  "load_emerge_config"
			  ),
			  PyTuple_New(0),
			  kwargs
		  )
  );

  Py_DECREF(kwargs);
  Py_DECREF(syncOpts);
  Py_DECREF(args);
  return 0;
}
