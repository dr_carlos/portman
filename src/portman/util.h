/*
 *  util.h
 *
 *  Copyright (c) 2006-2022 Pacman Development Team
 * <pacman-dev@lists.archlinux.org> Copyright (c) 2002-2006 by Judd Vinet
 * <jvinet@zeroflux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h> /* open, close */

#include "list.h"

#include "log.h"
#include "util-common.h"

#define CURSOR_HIDE_ANSICODE "\x1B[?25l"
#define CURSOR_SHOW_ANSICODE "\x1B[?25h"

#define MALLOC(p, s, action)                                                   \
  do {                                                                         \
    p = malloc(s);                                                             \
    if (p == NULL) {                                                           \
      fprintf(stderr, "malloc failure: could not allocate %zu bytes\n", s);    \
      action;                                                                  \
    }                                                                          \
  } while (0)

#define OPEN(fd, path, flags)                                                  \
  do {                                                                         \
    fd = open(path, flags | O_BINARY);                                         \
  } while (fd == -1 && errno == EINTR)

#define ASSERT(cond, action)                                                   \
  do {                                                                         \
    if (!(cond)) {                                                             \
      action;                                                                  \
    }                                                                          \
  } while (0)

/* check exported library symbols with: nm -C -D <lib> */
#define SYMEXPORT __attribute__((visibility("default")))

#ifndef O_BINARY
#define O_BINARY 0
#endif

#ifdef ENABLE_NLS
#include <libintl.h> /* here so it doesn't need to be included elsewhere */
/* define _() as shortcut for gettext() */
#define _(str) gettext(str)
#define _n(str1, str2, ct) ngettext(str1, str2, ct)
#else
#define _(str) (char *)str
#define _n(str1, str2, ct) (char *)(ct == 1 ? str1 : str2)
#endif

void trans_init_error(void);
/* flags is a bitfield of alpm_transflag_t flags */
int trans_init(int flags, int check_valid);
int trans_release(void);
int needs_root(void);
int check_syncdbs(size_t need_repos, int check_valid);
int sync_syncdbs(int level, list_t *syncs);
unsigned short getcols(void);
void columns_cache_reset(void);
int rmrf(const char *path);
void indentprint(const char *str, unsigned short indent, unsigned short cols);
char *strreplace(const char *str, const char *needle, const char *replace);
void string_display(const char *title, const char *string, unsigned short cols);
void string_display_mut(const char *title, char *string, unsigned short cols);
double humanize_size(off_t bytes, const char target_unit, int precision,
                     const char **label);
void list_display(const char *title, list_t *list,
                  unsigned short maxcols);
void list_display_linebreak(const char *title, const list_t *list,
                            unsigned short maxcols);
void display_targets(void);
int str_cmp(const void *s1, const void *s2);
void print_packages(const list_t *packages);
void select_display(const list_t *pkglist);
int select_question(int count);
int multiselect_question(char *array, int count);
int colon_printf(const char *format, ...) __attribute__((format(printf, 1, 2)));
int yesno(const char *format, ...) __attribute__((format(printf, 1, 2)));
int noyes(const char *format, ...) __attribute__((format(printf, 1, 2)));
char *arg_to_string(int argc, char *argv[]);
char *safe_fgets_stdin(char *s, int size);
void console_cursor_hide(void);
void console_cursor_show(void);
void console_cursor_move_up(unsigned int lines);
void console_cursor_move_down(unsigned int lines);
void console_cursor_move_end(void);
/* Erases line from the current cursor position till the end of the line */
void console_erase_line(void);

int pm_printf(alpm_loglevel_t level, const char *format, ...)
    __attribute__((format(printf, 2, 3)));
int pm_asprintf(char **string, const char *format, ...)
    __attribute__((format(printf, 2, 3)));
int pm_vfprintf(FILE *stream, alpm_loglevel_t level, const char *format,
                va_list args) __attribute__((format(printf, 3, 0)));
int pm_sprintf(char **string, alpm_loglevel_t level, const char *format, ...)
    __attribute__((format(printf, 3, 4)));
int pm_vasprintf(char **string, alpm_loglevel_t level, const char *format,
                 va_list args) __attribute__((format(printf, 3, 0)));
